# Gather tracing from Kubernetes Container

Tracing for the Trust software is recorded in files on the Trust software container. 
These files can be found in the `/usr/bin/xand/traces` path with the names `opentelemetry.log.*` and `tracedata.log.*`.
These files follow standard log file numbering, where `tracedata.log.3` is older than `tracedata.log.2` and 
`tracedata.log` is the active log file.

To copy the log files to your local machine, ensure you have `kubectl` configured for your Trust software cluster.

Identify your `trustee` pod name with:
```sh
> kubectl get pods -n xand
```
which will give you output similar to
```
NAME                         READY   STATUS    RESTARTS   AGE
trustee-6x6c96a88c-p9k29     2/2     Running   0          6d
...
```

You will then target that `trustee` pod to copy your tracedata. 

You will also want to choose an output directory on your local machine, e.g.:

```
export TRACE_DIR="/home/user/tracedata"
```

and then run

```
kubectl cp -n xand <INSERT/AUTOCOMPLETE TRUSTEE POD NAME>:/usr/bin/xand/traces $TRACE_DIR -c trustee
```

This will copy all the contents into `$TRACE_DIR` on your local machine.

