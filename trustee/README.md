# Trust Config Setup and Deployment

## Setting up dev config and secrets

All of the files needed to configure the Trust node are located in the `./config` directory. 

The files in the `./config/*` directories contain sensible default values that can be overridden 
as needed, like bank display names and routing numbers. As of 5/12/20, they are the default values expected
for deployed _dev_ networks. 

Files within `./config/base/` are uploaded as K8s `ConfigMap`, and files within `./config/secrets` are uploaded as K8s `Secret`.

These files contain more detailed instructions as inline comments on any more specific values that may need to be replaced.

## Deploying

Set the image versions, build, and apply

```
# This namespace should already exist from other deployments to this namespace.
NAMESPACE="<your namespace here>"

# Versions
TRUSTEE_STABLE_VERSION="<trustee image version>"
PROMETHEUS_STABLE_VERSION="<prometheus pipeserv version>"

# Set image versions
TRUST_IMAGE="gcr.io/xand-dev/trust:$TRUSTEE_STABLE_VERSION"
PROMETHEUS_PIPE_SRV_IMAGE="gcr.io/xand-dev/prometheus-pipesrv:$PROMETHEUS_STABLE_VERSION"

kustomize edit set image trust=$TRUST_IMAGE
kustomize edit set image prometheus-pipesrv=$PROMETHEUS_PIPE_SRV_IMAGE

# Set namespace
kustomize edit set namespace $NAMESPACE

# Build application.yaml file
kustomize build > application.yaml
```

and then 
```
kubectl apply -f application.yaml
```

### Tracing Data

Follow [these](GATHER_TRACE_LOGS.md) instructions to retrieve tracing data from your Trustee software deployment.
