#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat <<END
    This script is intended to be run by other scripts
    The purpose of this script is to retrieve the vault secrets for a given environment and place them
    in the appropriate paths to be picked up by kustomize to create secrets in kubernetes for wallet key secrets.

    Arguments
        ENVIRONMENT (Required) = The environment name that should be specified for the location in vault of where to look up secrets from.
END
)
echo "$HELPTEXT"
}

if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

ENVIRONMENT=$1

case $ENVIRONMENT in
     dev)
          ;;
     staging)
          ;;
     prod)
          ;;
     *)
          echo "ERROR: The environment $ENVIRONMENT passed in doesn't match a known environment in the script."
          exit 1
          ;;
esac

# Makes a json file in the format that the key manager is expecting.
function make_keypair_json {
    ADDRESS=$1
    PRIVATE_KEY=$2

    jq -n --arg PUBLIC_KEY $ADDRESS --arg PRIVATE_KEY "$PRIVATE_KEY" '{ "public_key": $PUBLIC_KEY, "private_key": $PRIVATE_KEY, "key_type": "SubstrateSr25519" }'
}

function make_encryption_key_json {
    PUBLIC=$1
    PRIVATE=$2

    jq -n --arg PUBLIC_KEY $PUBLIC --arg PRIVATE_KEY "$PRIVATE" '{ "public_key": $PUBLIC_KEY, "private_key": $PRIVATE_KEY, "key_type": "SharedEncryptionX25519" }'
}

AWESOMECO_PUBLIC_ADDRESS=$(vault kv get --field=public-address /secret/environments/$ENVIRONMENT/authorized-participants/awesomeco)
AWESOMECO_PRIVATE_KEY=$(vault kv get --field=private-key /secret/environments/$ENVIRONMENT/authorized-participants/awesomeco)
make_keypair_json $AWESOMECO_PUBLIC_ADDRESS "$AWESOMECO_PRIVATE_KEY" > namespaced/kp-$AWESOMECO_PUBLIC_ADDRESS

echo "Created awesomeco key file: namespaced/kp-$AWESOMECO_PUBLIC_ADDRESS"

AWESOMECO_ENCRYPTION_PUBKEY=$(vault kv get --field=encryption-pubkey /secret/environments/$ENVIRONMENT/authorized-participants/awesomeco)
AWESOMECO_ENCRYPTION_PRIVATE_KEY=$(vault kv get --field=encryption-private-key /secret/environments/$ENVIRONMENT/authorized-participants/awesomeco)
make_encryption_key_json "$AWESOMECO_ENCRYPTION_PUBKEY" "$AWESOMECO_ENCRYPTION_PRIVATE_KEY" > namespaced/kp-$AWESOMECO_ENCRYPTION_PUBKEY

echo "Created awesomeco encryption key file: namespaced/kp-$AWESOMECO_ENCRYPTION_PUBKEY"

COOLINC_PUBLIC_ADDRESS=$(vault kv get --field=public-address /secret/environments/$ENVIRONMENT/authorized-participants/cool-inc)
COOLINC_PRIVATE_KEY=$(vault kv get --field=private-key /secret/environments/$ENVIRONMENT/authorized-participants/cool-inc)
make_keypair_json $COOLINC_PUBLIC_ADDRESS "$COOLINC_PRIVATE_KEY" > namespaced/kp-$COOLINC_PUBLIC_ADDRESS

echo "Created coolinc key file: namespaced/kp-$COOLINC_PUBLIC_ADDRESS"

COOLINC_ENCRYPTION_PUBKEY=$(vault kv get --field=encryption-pubkey /secret/environments/$ENVIRONMENT/authorized-participants/cool-inc)
COOLINC_ENCRYPTION_PRIVATE_KEY=$(vault kv get --field=encryption-private-key /secret/environments/$ENVIRONMENT/authorized-participants/cool-inc)
make_encryption_key_json $COOLINC_ENCRYPTION_PUBKEY "$COOLINC_ENCRYPTION_PRIVATE_KEY" > namespaced/kp-$COOLINC_ENCRYPTION_PUBKEY

echo "Created coolinc encryption key file: namespaced/kp-$COOLINC_ENCRYPTION_PUBKEY"

TRUST_PUBLIC_ADDRESS=$(vault kv get --field=public-address /secret/environments/$ENVIRONMENT/trustee)
TRUST_PRIVATE_KEY=$(vault kv get --field=private-key /secret/environments/$ENVIRONMENT/trustee)
make_keypair_json $TRUST_PUBLIC_ADDRESS "$TRUST_PRIVATE_KEY" > namespaced/kp-$TRUST_PUBLIC_ADDRESS

echo "Created trust key file: namespaced/kp-$TRUST_PUBLIC_ADDRESS"

TRUST_ENCRYPTION_PUBKEY=$(vault kv get --field=encryption-pubkey /secret/environments/$ENVIRONMENT/trustee)
TRUST_ENCRYPTION_PRIVATE_KEY=$(vault kv get --field=encryption-private-key /secret/environments/$ENVIRONMENT/trustee)
make_encryption_key_json $TRUST_ENCRYPTION_PUBKEY "$TRUST_ENCRYPTION_PRIVATE_KEY" > namespaced/kp-$TRUST_ENCRYPTION_PUBKEY

echo "Created trust encryption key file: namespaced/kp-$TRUST_ENCRYPTION_PUBKEY"

LIMITED_AGENT_PUBLIC_ADDRESS=$(vault kv get --field=public-address /secret/environments/$ENVIRONMENT/limited-agent)
LIMITED_AGENT_PRIVATE_KEY=$(vault kv get --field=private-key /secret/environments/$ENVIRONMENT/limited-agent)
make_keypair_json $LIMITED_AGENT_PUBLIC_ADDRESS "$LIMITED_AGENT_PRIVATE_KEY" > namespaced/kp-$LIMITED_AGENT_PUBLIC_ADDRESS

echo "Created limited agent key file: namespaced/kp-$LIMITED_AGENT_PUBLIC_ADDRESS"

# Creating a file with all the public addresses

printf "TRUST_PUBLIC_ADDRESS=$TRUST_PUBLIC_ADDRESS\n" > namespaced/public-addresses.properties
printf "AWESOMECO_PUBLIC_ADDRESS=$AWESOMECO_PUBLIC_ADDRESS\n" >>namespaced/public-addresses.properties
printf "COOLINC_PUBLIC_ADDRESS=$COOLINC_PUBLIC_ADDRESS\n" >> namespaced/public-addresses.properties
printf "LIMITED_AGENT_PUBLIC_ADDRESS=$LIMITED_AGENT_PUBLIC_ADDRESS\n" >> namespaced/public-addresses.properties

echo "Created public-addresses.properties file."

for i in `seq 0 4`;
do
  VALIDATOR_PUBLIC_ADDRESS=$(vault kv get --field=authority-address /secret/environments/$ENVIRONMENT/validator-$i)
  VALIDATOR_PRIVATE_KEY=$(vault kv get --field=authority-private-key /secret/environments/$ENVIRONMENT/validator-$i)
  make_keypair_json $VALIDATOR_PUBLIC_ADDRESS "$VALIDATOR_PRIVATE_KEY" > namespaced/kp-$VALIDATOR_PUBLIC_ADDRESS

  echo "Created validator-$i key file: namespaced/kp-$VALIDATOR_PUBLIC_ADDRESS"

  VALIDATOR_ENCRYPTION_PUBKEY=$(vault kv get --field=encryption-pubkey /secret/environments/$ENVIRONMENT/validator-$i)
  VALIDATOR_ENCRYPTION_PRIVATE_KEY=$(vault kv get --field=encryption-private-key /secret/environments/$ENVIRONMENT/validator-$i)
  make_encryption_key_json $VALIDATOR_ENCRYPTION_PUBKEY "$VALIDATOR_ENCRYPTION_PRIVATE_KEY" > namespaced/kp-$VALIDATOR_ENCRYPTION_PUBKEY

  echo "Created validator-$i encryption key file: namespaced/kp-$VALIDATOR_ENCRYPTION_PUBKEY"

  pushd namespaced

  kustomize edit add secret validator-$i-xand-keys \
    --from-file=kp-$VALIDATOR_PUBLIC_ADDRESS \
    --from-file=kp-$VALIDATOR_ENCRYPTION_PUBKEY

  popd
done

pushd namespaced

kustomize edit add secret validator-0-xand-keys \
    --from-file=kp-$AWESOMECO_PUBLIC_ADDRESS \
    --from-file=kp-$AWESOMECO_ENCRYPTION_PUBKEY

kustomize edit add secret validator-1-xand-keys \
    --from-file=kp-$COOLINC_PUBLIC_ADDRESS \
    --from-file=kp-$COOLINC_ENCRYPTION_PUBKEY

kustomize edit add secret validator-2-xand-keys \
    --from-file=kp-$TRUST_PUBLIC_ADDRESS \
    --from-file=kp-$TRUST_ENCRYPTION_PUBKEY \

kustomize edit add secret validator-3-xand-keys \
    --from-file=kp-$AWESOMECO_PUBLIC_ADDRESS \
    --from-file=kp-$AWESOMECO_ENCRYPTION_PUBKEY \
    --from-file=kp-$COOLINC_PUBLIC_ADDRESS \
    --from-file=kp-$COOLINC_ENCRYPTION_PUBKEY \
    --from-file=kp-$TRUST_PUBLIC_ADDRESS \
    --from-file=kp-$TRUST_ENCRYPTION_PUBKEY \
    --from-file=kp-$LIMITED_AGENT_PUBLIC_ADDRESS

kustomize edit add secret validator-4-xand-keys \
    --from-file=kp-$AWESOMECO_PUBLIC_ADDRESS \
    --from-file=kp-$AWESOMECO_ENCRYPTION_PUBKEY \
    --from-file=kp-$COOLINC_PUBLIC_ADDRESS \
    --from-file=kp-$COOLINC_ENCRYPTION_PUBKEY \
    --from-file=kp-$TRUST_PUBLIC_ADDRESS \
    --from-file=kp-$TRUST_ENCRYPTION_PUBKEY \
    --from-file=kp-$LIMITED_AGENT_PUBLIC_ADDRESS

popd
