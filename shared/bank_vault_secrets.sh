#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat <<END
    This script is intended to be run by other scripts
    The purpose of this script is to retrieve the vault secrets for a given environment and place them
    in the appropriate paths to be picked up by kustomize to create secrets in kubernetes for bank secrets.

    Arguments
        ENVIRONMENT (Required) = The environment name that should be specified for the location in vault of where to look up secrets from.
END
)
echo "$HELPTEXT"
}

if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

ENVIRONMENT=$1

case $ENVIRONMENT in
     dev)
          ;;
     staging)
          ;;
     prod)
          ;;
     *)
          echo "ERROR: The environment $ENVIRONMENT passed in doesn't match a known environment in the script."
          exit 1
          ;;
esac

AWESOMECO_SG_ACCOUNT_NUMBER=$(vault kv get --field=sg-account-number /secret/environments/$ENVIRONMENT/authorized-participants/awesomeco)
AWESOMECO_MCB_ACCOUNT_NUMBER=$(vault kv get --field=mcb-account-number /secret/environments/$ENVIRONMENT/authorized-participants/awesomeco)
AWESOMECO_PROVIDENT_ACCOUNT_NUMBER=$(vault kv get --field=provident-account-number /secret/environments/$ENVIRONMENT/authorized-participants/awesomeco)

printf "SG_ACCOUNT_NUMBER=$AWESOMECO_SG_ACCOUNT_NUMBER\n" > namespaced/awesomeco-banks.env
printf "MCB_ACCOUNT_NUMBER=$AWESOMECO_MCB_ACCOUNT_NUMBER\n" >> namespaced/awesomeco-banks.env
printf "PROVIDENT_ACCOUNT_NUMBER=$AWESOMECO_PROVIDENT_ACCOUNT_NUMBER\n" >> namespaced/awesomeco-banks.env

echo "Created awesomeco-banks.env file."

COOLINC_SG_ACCOUNT_NUMBER=$(vault kv get --field=sg-account-number /secret/environments/$ENVIRONMENT/authorized-participants/cool-inc)
COOLINC_MCB_ACCOUNT_NUMBER=$(vault kv get --field=mcb-account-number /secret/environments/$ENVIRONMENT/authorized-participants/cool-inc)
COOLINC_PROVIDENT_ACCOUNT_NUMBER=$(vault kv get --field=provident-account-number /secret/environments/$ENVIRONMENT/authorized-participants/cool-inc)
printf "SG_ACCOUNT_NUMBER=$COOLINC_SG_ACCOUNT_NUMBER\n" > namespaced/coolinc-banks.env
printf "MCB_ACCOUNT_NUMBER=$COOLINC_MCB_ACCOUNT_NUMBER\n" >> namespaced/coolinc-banks.env
printf "PROVIDENT_ACCOUNT_NUMBER=$COOLINC_PROVIDENT_ACCOUNT_NUMBER\n" >> namespaced/coolinc-banks.env

echo "Created coolinc-banks.env file."

TRUST_SG_ACCOUNT_NUMBER=$(vault kv get --field=sg-account-number /secret/environments/$ENVIRONMENT/trustee)
TRUST_MCB_ACCOUNT_NUMBER=$(vault kv get --field=mcb-account-number /secret/environments/$ENVIRONMENT/trustee)
TRUST_PROVIDENT_ACCOUNT_NUMBER=$(vault kv get --field=provident-account-number /secret/environments/$ENVIRONMENT/trustee)
printf "SG_ACCOUNT_NUMBER=$TRUST_SG_ACCOUNT_NUMBER\n" > namespaced/trust-banks.env
printf "MCB_ACCOUNT_NUMBER=$TRUST_MCB_ACCOUNT_NUMBER\n" >> namespaced/trust-banks.env
printf "PROVIDENT_ACCOUNT_NUMBER=$TRUST_PROVIDENT_ACCOUNT_NUMBER\n" >> namespaced/trust-banks.env

echo "Created trust-banks.env file."

vault kv get --field=value /secret/environments/$ENVIRONMENT/silvergate/bank-cert-pfx > namespaced/sg_cert.pfx

BANK_CERT_PASSPHRASE=$(vault kv get --field=value /secret/environments/$ENVIRONMENT/silvergate/bank-cert-pw)
BANK_SUBSCRIPTION_KEY=$(vault kv get --field=value /secret/environments/$ENVIRONMENT/silvergate/bank-api-key)
BANK_TRUST_SUBSCRIPTION_KEY=$(vault kv get --field=value /secret/environments/$ENVIRONMENT/silvergate/trustee-bank-api-key)
printf ".bankcertpass=$BANK_CERT_PASSPHRASE\n.subscription-key=$BANK_SUBSCRIPTION_KEY\n.trust-subscription-key=$BANK_TRUST_SUBSCRIPTION_KEY" > namespaced/silvergate-bank-passphrases.env

echo "Created silvergate-bank-passphrases.env file."

BANK_MCB_USERNAME=$(vault kv get --field=username /secret/environments/$ENVIRONMENT/mcb/bank-creds)
BANK_MCB_PASSWORD=$(vault kv get --field=password /secret/environments/$ENVIRONMENT/mcb/bank-creds)
printf ".bank_mcb_username=$BANK_MCB_USERNAME\n.bank_mcb_password=$BANK_MCB_PASSWORD" > namespaced/mcb-bank-passphrases.env

BANK_MCB_CLIENT_APP_IDENT=$(vault kv get --field=client_app_ident /secret/environments/$ENVIRONMENT/mcb/identities)
BANK_MCB_ORGANIZATION_ID=$(vault kv get --field=organization_id /secret/environments/$ENVIRONMENT/mcb/identities)
printf ".bank_mcb_client_app_ident=$BANK_MCB_CLIENT_APP_IDENT\n.bank_mcb_organization_id=$BANK_MCB_ORGANIZATION_ID" > namespaced/mcb-bank-identities.env

echo "Created mcb-bank-passphrases.env file."

BANK_PROVIDENT_USERNAME=$(vault kv get --field=username /secret/environments/$ENVIRONMENT/provident/bank-creds)
BANK_PROVIDENT_PASSWORD=$(vault kv get --field=password /secret/environments/$ENVIRONMENT/provident/bank-creds)
printf ".bank_provident_username=$BANK_PROVIDENT_USERNAME\n.bank_provident_password=$BANK_PROVIDENT_PASSWORD" > namespaced/provident-bank-passphrases.env

echo "Created provident-bank-passphrases.env file."

echo "Completed retreiving banks secrets."
