This directory contains configurations for a develoment `member-api` Application in K8s.

Pull a copy of the `member-api-k8s` package, and _deploy_ to a namespace with

```
K8S_NAMESPACE=<value>

ZIP_FILENAME="member-api-k8s.1.3.2.zip"
ARTIFACT_URL="https://transparentinc.jfrog.io/artifactory/artifacts-internal/member-api-k8s/${ZIP_FILENAME}"
echo $ARTIFACT_URL
curl -u${ARTIFACTORY_USER}:${ARTIFACTORY_PASS} -O ${ARTIFACT_URL}

GCR_MEMBER_API_IMAGE="gcr.io/xand-dev/member-api:1.3.2"

./deploy.sh ${ZIP_FILENAME} ${GCR_MEMBER_API_IMAGE} ${K8S_NAMESPACE}
```
