#!/bin/sh

##  Kubeval is used to validate one or more Kubernetes configuration files, and is often used locally as part of a development workflow as well as in CI pipelines.
##  https://kubeval.instrumenta.dev
##
##    --ignore-missing-schemas      Skip validation for resource definitions without a schema
##
##  Only perform validation on k8s yaml files excluding the kustomization.yaml, gitlab
##  and gitlab yaml files as these don't apply.
##  When an errors are encountered the script exits with error code of 1 (will cause CI pipeline to fail)
##
##     ...
##     PASS - ./test/namespace.yaml contains a valid Namespace (test)
##     PASS - ./test/member-api.test.deployment.yaml contains a valid Deployment (member-api)
##     PASS - ./trustee/trustee.deployment.yaml contains a valid Deployment (trustee)
##     PASS - ./trustee/trustee.deployment.yaml contains a valid Service (trustee)
##     ERR  - Failed to decode YAML from ./trustee/trustee.configmap.yaml: error converting YAML to JSON: yaml: line 16: did not find expected key
##
##     |jgrant| kraken in ~/projects/xand-k8s
##     [validate_yaml ⚠⚑] → echo $?
##     1

IGNORE_LIST="gitlab|patches|envoy_config|prometheus_alert_tests|docker-compose.yaml|secrets.yaml|config.yaml|banks.yaml|trustee/config/"

./bin/kubeval --ignore-missing-schemas \
              $(find . -type f \( -iname "*.yaml" ! -iname "kustomization*.yaml" ! -iname "*_patch.deployment.yaml" \) | grep -vE "${IGNORE_LIST}") \
              -i ./**/*docker-compose.yaml
