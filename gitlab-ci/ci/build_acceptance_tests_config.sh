#!/bin/bash

K8S_NAMESPACE=${1:?"$( error 'K8S_NAMESPACE must be set' )"}
VAULT_ENVIRONMENT=${2:?"$( error 'VAULT_ENVIRONMENT must be set' )"}

echo Retrieving vault validator public addresses

export VALIDATOR_0_PUBLIC_ADDRESS=$(vault kv get --field=authority-address /secret/environments/${VAULT_ENVIRONMENT}/validator-0)
export VALIDATOR_0_XAND_API_CONNECTION="https://xand-api-0.${K8S_NAMESPACE}.reviewapp.xand.tools:443"

echo "VALIDATOR_0_PUBLIC_ADDRESS           = ${VALIDATOR_0_PUBLIC_ADDRESS}"
echo "VALIDATOR_0_XAND_API_CONNECTION      = ${VALIDATOR_0_XAND_API_CONNECTION}"

export VALIDATOR_1_PUBLIC_ADDRESS=$(vault kv get --field=authority-address /secret/environments/${VAULT_ENVIRONMENT}/validator-1)
export VALIDATOR_1_XAND_API_CONNECTION="https://xand-api-1.${K8S_NAMESPACE}.reviewapp.xand.tools:443"

echo "VALIDATOR_1_PUBLIC_ADDRESS           = ${VALIDATOR_1_PUBLIC_ADDRESS}"
echo "VALIDATOR_1_XAND_API_CONNECTION      = ${VALIDATOR_1_XAND_API_CONNECTION}"

export VALIDATOR_2_PUBLIC_ADDRESS=$(vault kv get --field=authority-address /secret/environments/${VAULT_ENVIRONMENT}/validator-2)
export VALIDATOR_2_XAND_API_CONNECTION="https://xand-api-2.${K8S_NAMESPACE}.reviewapp.xand.tools:443"

echo "VALIDATOR_2_PUBLIC_ADDRESS           = ${VALIDATOR_2_PUBLIC_ADDRESS}"
echo "VALIDATOR_2_XAND_API_CONNECTION      = ${VALIDATOR_2_XAND_API_CONNECTION}"

export VALIDATOR_3_PUBLIC_ADDRESS=$(vault kv get --field=authority-address /secret/environments/${VAULT_ENVIRONMENT}/validator-3)
export VALIDATOR_3_XAND_API_CONNECTION="https://xand-api-3.${K8S_NAMESPACE}.reviewapp.xand.tools:443"

echo "VALIDATOR_3_PUBLIC_ADDRESS           = ${VALIDATOR_3_PUBLIC_ADDRESS}"
echo "VALIDATOR_3_XAND_API_CONNEC4ION      = ${VALIDATOR_3_XAND_API_CONNEC4ION}"

export VALIDATOR_4_PUBLIC_ADDRESS=$(vault kv get --field=authority-address /secret/environments/${VAULT_ENVIRONMENT}/validator-4)
export VALIDATOR_4_XAND_API_CONNECTION="https://xand-api-4.${K8S_NAMESPACE}.reviewapp.xand.tools:443"

echo "VALIDATOR_4_PUBLIC_ADDRESS           = ${VALIDATOR_4_PUBLIC_ADDRESS}"
echo "VALIDATOR_4_XAND_API_CONNECTION      = ${VALIDATOR_4_XAND_API_CONNECTION}"

echo Retrieving vault member public addresses

export AWESOMECO_PUBLIC_ADDRESS=$(vault kv get --field=public-address /secret/environments/${VAULT_ENVIRONMENT}/authorized-participants/awesomeco)
export AWESOMECO_MEMBER_API_URL="https://member-api-0.${K8S_NAMESPACE}.reviewapp.xand.tools"
export AWESOMECO_XAND_API_CONNECTION=$VALIDATOR_0_XAND_API_CONNECTION

echo "AWESOMECO_PUBLIC_ADDRESS           = ${AWESOMECO_PUBLIC_ADDRESS}"
echo "AWESOMECO_MEMBER_API_URL           = ${AWESOMECO_MEMBER_API_URL}"
echo "AWESOMECO_XAND_API_CONNECTION      = ${AWESOMECO_XAND_API_CONNECTION}"

export COOLINC_PUBLIC_ADDRESS=$(vault kv get --field=public-address /secret/environments/${VAULT_ENVIRONMENT}/authorized-participants/cool-inc)
export COOLINC_MEMBER_API_URL"=https://member-api-1.${K8S_NAMESPACE}.reviewapp.xand.tools"
export COOLINC_XAND_API_CONNECTION=$VALIDATOR_1_XAND_API_CONNECTION

echo "COOLINC_PUBLIC_ADDRESS           = ${COOLINC_PUBLIC_ADDRESS}"
echo "COOLINC_MEMBER_API_URL           = ${COOLINC_MEMBER_API_URL}"
echo "COOLINC_XAND_API_CONNECTION      = ${COOLINC_XAND_API_CONNECTION}"

export TRUST_ADDRESS=$(vault kv get --field=public-address /secret/environments/${VAULT_ENVIRONMENT}/trustee)
export TRUST_API_URL="https://trust-api.${K8S_NAMESPACE}.reviewapp.xand.tools"
export TRUST_XAND_API_CONNECTION=$VALIDATOR_2_XAND_API_CONNECTION

echo "TRUST_ADDRESS                  = ${TRUST_ADDRESS}"
echo "TRUST_API_URL                  = ${TRUST_API_URL}"
echo "TRUST_XAND_API_CONNECTION      = ${TRUST_XAND_API_CONNECTION}"


# Wait for URLs to respond (insecure)
curl "${AWESOMECO_MEMBER_API_URL}/api/v1/health" --insecure --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${COOLINC_MEMBER_API_URL}/api/v1/health" --insecure --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${AWESOMECO_XAND_API_CONNECTION}" --insecure --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${COOLINC_XAND_API_CONNECTION}" --insecure --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${TRUST_XAND_API_CONNECTION}" --insecure --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${VALIDATOR_3_XAND_API_CONNECTION}" --insecure --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${VALIDATOR_4_XAND_API_CONNECTION}" --insecure --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "mcb-mock.${K8S_NAMESPACE}.reviewapp.xand.tools" -L --insecure --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "provident-mock.${K8S_NAMESPACE}.reviewapp.xand.tools" -L --insecure --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "sg-mock.${K8S_NAMESPACE}.reviewapp.xand.tools" -L --insecure --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${TRUST_API_URL}/service/health" --insecure --retry-connrefused --retry-delay 2 --retry 100 --output -

# Wait for URLs to respond (secure)
curl "${AWESOMECO_MEMBER_API_URL}/api/v1/health" --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${COOLINC_MEMBER_API_URL}/api/v1/health" --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${AWESOMECO_XAND_API_CONNECTION}" --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${COOLINC_XAND_API_CONNECTION}" --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${TRUST_XAND_API_CONNECTION}" --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${VALIDATOR_3_XAND_API_CONNECTION}" --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${VALIDATOR_4_XAND_API_CONNECTION}" --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "mcb-mock.${K8S_NAMESPACE}.reviewapp.xand.tools" -L --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "provident-mock.${K8S_NAMESPACE}.reviewapp.xand.tools" -L --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "sg-mock.${K8S_NAMESPACE}.reviewapp.xand.tools" -L --retry-connrefused --retry-delay 2 --retry 100 --output -
curl "${TRUST_API_URL}/service/health" --retry-connrefused --retry-delay 2 --retry 100 --output -

export MCB_BANK_MOCKS_URL="http://mcb-mock/metropolitan"
export PROV_BANK_MOCKS_URL="http://provident-mock/provident"

# Install gettext-base for the `envsubst` tool
apt-get install gettext-base -yqq
source .env

cd gitlab-ci/config

 # Sub in env vars to build config files
which envsubst
envsubst < entities-metadata.yaml.template > entities-metadata.yaml
cat entities-metadata.yaml
envsubst < config.yaml.template > config.yaml
cat config.yaml