#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat <<END
    This script is intended to be run by either humans or automation
    The purpose of this script is to perform the deployment of a review-app environment.

    Arguments
        CUSTOM_NAMESPACE (Required) = The name that should be used in creating this namespace.
        CUSTOM_URL_SLUG_SEGMENT (Required) = A short name that can be included in urls to uniquely identify this environment that is friendly for urls.
        CHAINSPEC_VERSION (Required) = The chain spec (dockerized) version to use for this deployment
        VALIDATOR_VERSION (Optional) = The validator version which will be used for this deployment.
END
)
echo "$HELPTEXT"
}

# Prints helptext if no arguments were passed in even though they are all
# optional just to allow a user to notice if they did not intend to run the
# script without parameters.
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

CUSTOM_NAMESPACE=$1

# Check if arg was passed in
if [[ -z $CUSTOM_NAMESPACE ]] ; then
    echo "ERROR: No CUSTOM_NAMESPACE passed in as the first arg."
    exit 1
fi

CUSTOM_URL_SLUG_SEGMENT=$2

# Check if arg was passed in
if [[ -z $CUSTOM_URL_SLUG_SEGMENT ]] ; then
    echo "ERROR: No CUSTOM_URL_SLUG_SEGMENT passed in as the second arg."
    exit 1
fi

CHAINSPEC_VERSION=$3
# Check if arg was passed in
if [[ -z $CHAINSPEC_VERSION ]] ; then
    echo "WARNING: No CHAINSPEC_VERSION passed in as the third arg."
    exit 1
fi

VALIDATOR_VERSION=$4

# Check if arg was passed in
if [[ -z $VALIDATOR_VERSION ]] ; then
    echo "WARNING: No VALIDATOR_VERSION passed in as the third arg."
fi

pushd ingresses
for TEMPLATE_FILE in $(ls *.yaml.template)
do
    OUTPUT_FILE_NAME=$(basename $TEMPLATE_FILE .template)

    # Perform Transformations on ingress templates here.
    cat $TEMPLATE_FILE |
        sed -e "s|\${BRANCH_SLUG_NAME}|${CUSTOM_URL_SLUG_SEGMENT}|g" > $OUTPUT_FILE_NAME
done
popd

./vault_secrets.sh
../shared/deploy.sh $CHAINSPEC_VERSION $VALIDATOR_VERSION $CUSTOM_NAMESPACE
