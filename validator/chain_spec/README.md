# Generating a Chain Spec

Overview:

1. Set up an `$IDENTITIES_DIR` directory containing the data for all entities in the chain spec

1. Set up vars and invoke script produce and publish chainspec

## Filling out Network Identities

A chain spec (genesis block) must reference
- 1 Trust Identity
- 1 Limited Agent
- N Members
- M Validators

Build a yaml config file specifying each of these entity's identifying info.

See [`sample_chain_spec_config.yaml`](sample_chain_spec_config.yaml) for reference.

## Produce and Publish Chain Spec

A helper bash script has been created to produce and publish the chainspec file.

These are the arguments to the scripts in order of being passed

* CHAINSPEC_GENERATOR_CONFIG (Required) = Path to config file with entity's identities
* DOCKER_REGISTRY (Required) = Docker registry to pull/push from. One of ["gcr.io/xand-dev", "transparentinc-docker-external.jfrog.io"]
* VALIDATOR_DOCKER_IMAGE_VERSION_ARG (Required) = Version of the docker image
* OUTPUT_DIR_ARG (Required) = Path to write chainspec intermediate files out to.
* TAG_PREFIX_ARG (Optional) = Optionally prefix the docker image tag. Base tag is the current timestamp (UTC)
* TAG_OVERRIDE (Optional) = Override default tag
```shell
CHAINSPEC_GENERATOR_CONFIG="sample_chain_spec_config.yaml"
source ../../.env

./produce_and_publish_chain_spec.sh \
  $CHAINSPEC_GENERATOR_CONFIG \
  "transparentinc-docker-external.jfrog.io" \
  $VALIDATOR_STABLE_VERSION \
  ./output \
  sgd
```

The script will optionally publish the resulting image to the specified registry.
