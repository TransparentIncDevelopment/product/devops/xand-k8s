#!/usr/bin/env bash

# Uncomment to debug
#set -x

set -e # Exit on error
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -o pipefail

function helptext {
HELPTEXT="
    This script is intended to be run by automation or humans. Given a vault env, fetch identity
    values and associated IP addresses to build for downstream parsing as input to build a chain spec

	ENVIRONMENT (Required) = Vault environment
	OUT_FILE (Required) = YAML file to write identity structure and values
"
echo "$HELPTEXT"
}

function error {
    echo "$1"
    exit 1
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    error "$(helptext)

Missing arguments"
fi

# Verify args are set
ENVIRONMENT=${1:?"$( error 'ENVIRONMENT must be set' )"}
OUT_FILE=${2:?"$( error 'OUT_FILE must be set' )"}

OUT_FILE_ABS=$(realpath $OUT_FILE)

echo "Using vault environment: ${ENVIRONMENT}"
echo "Using output file: ${OUT_FILE_ABS}"

echo "Fetching CIDR_BLOCKS SET"

CIDR_BLOCKS=$(vault kv get --field=allowed /secret/environments/$ENVIRONMENT/cidr_authorization)
# Adds quotes around values to make 127.0.0.1/32,127.0.0.1/32 -> "127.0.0.1/32","127.0.0.1/32"
CIDR_BLOCKS_QUOTED=$(echo $CIDR_BLOCKS | sed -e 's/,/","/g' | sed -e 's/^/"/g' | sed -e 's/$/"/g' )
echo $CIDR_BLOCKS_QUOTED

# Note:
# Only assigning CIDR_BLOCK to first member identity to keep consistent with previous commit.
# See template: https://gitlab.com/TransparentIncDevelopment/product/devops/xand-k8s/-/blob/5fa427c5ff09dff011f39b491b4d309842f34b4e/validator/genesis_runtime_configs/xandstrate_config.json
# TODO: Test with multiple urls per identity

# JSON templates for building identity JSON configs
TRUST_TEMPLATE=$(cat <<-EOF
trust:
  address: %s
  ip_address_ranges: [%s]
  encryption_pub_key: %s
EOF
)

LIMITED_AGENT_TEMPLATE=$(cat <<-EOF
limited_agent:
  address: %s
  ip_address_ranges: [%s]
EOF
)

TWO_MEMBERS_TEMPLATE=$(cat <<-EOF
members:
  - address: %s
    ip_address_ranges: [%s]
    encryption_pub_key: %s
  - address: %s
    ip_address_ranges: [%s]
    encryption_pub_key: %s
EOF
)


# Fetch member metadata
echo "Fetching and preparing members"
AWESOMECO=$(vault kv get --field=public-address /secret/environments/$ENVIRONMENT/authorized-participants/awesomeco)
COOL_INC=$(vault kv get --field=public-address /secret/environments/$ENVIRONMENT/authorized-participants/cool-inc)
AWESOMECO_ENCRYPTION_KEY=$(vault kv get --field=encryption-pubkey /secret/environments/$ENVIRONMENT/authorized-participants/awesomeco)
COOL_INC_ENCRYPTION_KEY=$(vault kv get --field=encryption-pubkey /secret/environments/$ENVIRONMENT/authorized-participants/cool-inc)

MEMBER_YAML_FRAGMENT=$(\
  printf "${TWO_MEMBERS_TEMPLATE}"\
  "${AWESOMECO}" "${CIDR_BLOCKS_QUOTED}" "${AWESOMECO_ENCRYPTION_KEY}"\
  "${COOL_INC}" "" "${COOL_INC_ENCRYPTION_KEY}"\
)

echo "Fetching and preparing trust"
TRUSTEE=$(vault kv get --field=public-address /secret/environments/$ENVIRONMENT/trustee)
TRUSTEE_ENCRYPTION_KEY=$(vault kv get --field=encryption-pubkey /secret/environments/$ENVIRONMENT/trustee)
TRUSTEE_YAML_FRAGMENT=$(printf "${TRUST_TEMPLATE}" "${TRUSTEE}" "" "${TRUSTEE_ENCRYPTION_KEY}")

echo "Fetching and preparing limited agent"
LIMITED_AGENT=$(vault kv get --field=public-address /secret/environments/$ENVIRONMENT/limited-agent)
LIMITED_AGENT_YAML_FRAGMENT=$(printf "${LIMITED_AGENT_TEMPLATE}" "${LIMITED_AGENT}" "")

echo "Fetching and preparing validators"

VALIDATOR_ENTRY_TEMPLATE=$(cat <<-EOF
  - address: %s
    aura_session_key: %s
    grandpa_session_key: %s
    encryption_pub_key: %s
    ip_address_ranges: []
EOF
)

VALIDATOR_YAML_FRAGMENT="validators:"

for i in `seq 0 4`;
do
  VALIDATOR_SESSION_AUTH_ADDRESS=$(vault kv get --field=authority-address /secret/environments/$ENVIRONMENT/validator-$i)
  VALIDATOR_SESSION_AURA_ADDRESS=$(vault kv get --field=aura-address /secret/environments/$ENVIRONMENT/validator-$i)
  VALIDATOR_SESSION_GRANDPA_ADDRESS=$(vault kv get --field=session-address /secret/environments/$ENVIRONMENT/validator-$i)
  VALIDATOR_ENCRYPTION_KEY=$(vault kv get --field=encryption-pubkey /secret/environments/$ENVIRONMENT/validator-$i)

  # TODO: Auth Identity is used for session as well. Test with separate value for aura session key
  ENTRY_YAML_FRAGMENT=$(printf "$VALIDATOR_ENTRY_TEMPLATE" \
      "$VALIDATOR_SESSION_AUTH_ADDRESS" \
      "$VALIDATOR_SESSION_AURA_ADDRESS" \
      "$VALIDATOR_SESSION_GRANDPA_ADDRESS" \
      "$VALIDATOR_ENCRYPTION_KEY"
  )
  VALIDATOR_YAML_FRAGMENT="$VALIDATOR_YAML_FRAGMENT"$'\n'"$ENTRY_YAML_FRAGMENT"
done

VALIDATOR_EMISSION_RATE_ENTRY_TEMPLATE=$(cat <<-EOF
validator_emission_rate:
  minor_units_per_validator_emission: %s
  block_quota: %s
EOF
)

DEFAULT_VALIDATOR_EMISSION_RATE_MINOR_UNITS=100
DEFAULT_VALIDATOR_EMISSION_BLOCK_QUOTA=2
VALIDATOR_EMISSION_RATE_MINOR_UNITS_FRAGMENT="$(printf "$VALIDATOR_EMISSION_RATE_ENTRY_TEMPLATE" \
                                                   "$DEFAULT_VALIDATOR_EMISSION_RATE_MINOR_UNITS" \
                                                   "$DEFAULT_VALIDATOR_EMISSION_BLOCK_QUOTA"
                                               )"

cat > $OUT_FILE_ABS <<EOF
$MEMBER_YAML_FRAGMENT

$TRUSTEE_YAML_FRAGMENT

$LIMITED_AGENT_YAML_FRAGMENT

$VALIDATOR_YAML_FRAGMENT

$VALIDATOR_EMISSION_RATE_MINOR_UNITS_FRAGMENT
EOF

echo "Done!"
