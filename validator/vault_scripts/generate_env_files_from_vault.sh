#!/usr/bin/env bash
set -e

# Trim trailing slash if exists
GENERATED_DIR=${1%/}

# Check if arg was passed in
if [[ -z $GENERATED_DIR ]] ; then
    echo "ERROR: The directory for generated artifacts needs to be passed in arg 1"
    exit 1
fi

# Check if arg is a directory
if ! [[ -d $GENERATED_DIR ]] ; then
    echo "ERROR: $GENERATED_DIR not found or is not a directory"
    exit 1
fi

ENVIRONMENT=$2

# Check if arg was passed in
if [[ -z $ENVIRONMENT ]] ; then
    echo "ERROR: The environment location in vault needs to be specified."
    exit 2
fi

case $ENVIRONMENT in
     dev)
          ;;
     staging)
          ;;
     prod)
          ;;
     *)
          echo "ERROR: The environment $ENVIRONMENT passed in doesn't match a known environment in the script."
          exit 2
          ;;
esac

GENERATED_VALIDATOR_ARTIFACTS_DIR=${GENERATED_DIR}/validatork8s
mkdir -p ${GENERATED_VALIDATOR_ARTIFACTS_DIR}

# The sequence here is to move through the static values defined in vault.
# When updating the number of validators in vault this sequence will need to be increased or decreased accordingly.
for i in `seq 0 4`
do
    VALIDATOR_I_ENV_LP2P_KEY_FILE=${GENERATED_VALIDATOR_ARTIFACTS_DIR}/validator-${i}-lp2p-key.env
    VALIDATOR_I_ENV_CONFIG_FILE=${GENERATED_VALIDATOR_ARTIFACTS_DIR}/validator-${i}-config.env

    NODE_KEY=$(vault kv get --field=node_key /secret/environments/$ENVIRONMENT/validator-${i})
    printf "NODE_KEY=$NODE_KEY\n" > ${VALIDATOR_I_ENV_LP2P_KEY_FILE}

    echo "Created $VALIDATOR_I_ENV_LP2P_KEY_FILE file."

    BOOTNODE_URL_VEC=$(vault kv get --field=bootnode_url_vec /secret/environments/$ENVIRONMENT/validator-${i})
    NODE_NAME=$(vault kv get --field=node_name /secret/environments/$ENVIRONMENT/validator-${i})

    printf "BOOTNODE_URL_VEC=$BOOTNODE_URL_VEC\nNODE_NAME=$NODE_NAME\n" > ${VALIDATOR_I_ENV_CONFIG_FILE}

    echo "Created $VALIDATOR_I_ENV_CONFIG_FILE file."

done
