#!/usr/bin/env bash

set -e

# Setup working directory for child scripts to do their work
GENERATED_WORKING_DIR=./generated
mkdir -p $GENERATED_WORKING_DIR

ENVIRONMENT=$1

# Check if arg was passed in
if [[ -z $ENVIRONMENT ]] ; then
    echo "ERROR: The environment location in vault needs to be specified."
    exit 1
fi

case $ENVIRONMENT in
     dev)
          ;;
     staging)
          ;;
     prod)
          ;;
     *)
          echo "ERROR: The environment $ENVIRONMENT passed in doesn't match a known environment in the script."
          exit 1
          ;;
esac

# Pull the vault values for the validators
${BASH_SOURCE%/*}/generate_env_files_from_vault.sh ${GENERATED_WORKING_DIR} ${ENVIRONMENT}
${BASH_SOURCE%/*}/generate_keyfiles_from_vault.sh ${GENERATED_WORKING_DIR} ${ENVIRONMENT}
