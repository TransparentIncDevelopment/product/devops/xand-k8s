#!/usr/bin/env bash
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

TEMP_DIR=$(mktemp -d)
./create_partner_artifacts.sh $@ $TEMP_DIR
pushd $TEMP_DIR
ls -al $TEMP_DIR
sed -i"" -e "s/{INSERT_URL_HERE}/xand-api\.example\.com/g" -e "s/{INSERT_SECRET_URL_HERE}/xand-api-example-com/g" ./validator.ingress.yaml
kustomize build . > application.yaml
popd
../bin/kubeval $TEMP_DIR/application.yaml
rm -rf $TEMP_DIR
